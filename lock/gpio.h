/* gpio.h
 *
 * Header file for gpio.c.
 * Steven Cacner <steven.cacner@me.com>
 *
 */

#ifndef GPIO_H_
#define GPIO_H_

int GPIOExport(int pin);

int GPIOUnexport(int pin);

int GPIODirection(int pin, int dir);

int GPIORead(int pin);

int GPIOWrite(int pin, int value);

#endif // GPIO_H_
