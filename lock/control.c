/* control.c
 *
 * For use in Door Lock Project.
 *
 * Controls lock by unlocking lock for X seconds, then locking lock again.
 * Steven Cacner <steven.cacner@me.com>
 *
 */

#include <stdio.h>
#include "gpio.h"

#define LOW  0
#define HIGH 1

#define POUT 26 /* PI-37 */

int
main(int argc, char *argv[])
{
   /*
    * Check for only one argument. If argument is not shown, print usage
    */
   if (argc != 2) {
      printf("usage: %s [number of seconds to keep lock unlocked]\n", argv[0]);
      return(-1);
   } else{
      int X = atoi(argv[1]);

      /*
       * Unlock: Write GPIO value HIGH
       */
      printf("Unlocking...\n");
      if (-1 == GPIOWrite(POUT, HIGH))
         return(-2);

      /*
       * Sleep for X seconds
       */
      usleep(X * 1000000);

      /*
       * Lock: Write GPIO value LOW
       */
      printf("Locking...\n");
      if (-1 == GPIOWrite(POUT, LOW))
         return(-3);

      return(0);
   }
}
