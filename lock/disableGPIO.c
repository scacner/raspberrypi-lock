/* enableGPIO.c
 *
 * Disable GPIO pins for use in Door Lock Project.
 * Steven Cacner <steven.cacner@me.com>
 *
 */

#include "gpio.h"

#define PIN  21 /* PI-40 */
#define POUT 26 /* PI-37 */

int
main(int argc, char *argv[])
{
   /*
    * Disable GPIO pins
    */
   if (-1 == GPIOUnexport(POUT) || -1 == GPIOUnexport(PIN))
      return(-1);

   return(0);
}
