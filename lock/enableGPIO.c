/* enableGPIO.c
 *
 * Enable GPIO pins for use in Door Lock Project.
 * Steven Cacner <steven.cacner@me.com>
 *
 */

#include "gpio.h"

#define IN  0
#define OUT 1

#define PIN  21 /* PI-40 */
#define POUT 26 /* PI-37 */

int
main(int argc, char *argv[])
{
   /*
    * Enable GPIO pins
    */
   if (-1 == GPIOExport(POUT) || -1 == GPIOExport(PIN))
      return(-1);

   /*
    * Set GPIO directions
    */
   if (-1 == GPIODirection(POUT,OUT) || -1 == GPIODirection(PIN, IN))
      return(-2);

   return(0);
}
