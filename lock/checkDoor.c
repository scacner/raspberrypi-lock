/* checkDoor.c
 *
 * For use in Door Lock Project.
 *
 * This checks GPIO pin PIN value, which corresponds to the door status.
 * Prints door status and returns pin value.
 * Steven Cacner <steven.cacner@me.com>
 *
 */

#include <stdio.h>
#include "gpio.h"

#define PIN  21 /* PI-40 */

int
main(int argc, char *argv[])
{
   int status;

   /*
    * Read GPIO value and put into variable status
    */
   status = GPIORead(PIN);
   if (0 == status) {
      printf("The door is closed.\n");
   } else {
      printf("The door is open.\n");
   }

   /*
    * Return status code of door
    */
   return(status);
}
